package rubricatelefonica;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collections;


import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class FormWindow {
	
	JDialog formDialog = new JDialog();
	Container formContainer = formDialog.getContentPane();
	JPanel panelMenu = new JPanel();
	JPanel panelForm = new JPanel();
	JPanel panelBottoni = new JPanel();
	JLabel labMenu = new JLabel("Inserimento dati");
	JButton accetta = new JButton("Accetta");
	JButton annulla = new JButton("Annulla");
	
	//campi dell'editor
	JLabel labelCognome = new JLabel("Cognome");   //cognome
	JTextField textCognome = new JTextField(50);
	JLabel labelNome = new JLabel("Nome");        //nome
	JTextField textNome = new JTextField(50);
	JLabel labelEta = new JLabel("Et�");         //eta
	JTextField textEta = new JTextField(50);
	JLabel labelIndirizzo = new JLabel("Indirizzo");     //indirizzo
	JTextField textIndirizzo = new JTextField(50);
	JLabel labelTel = new JLabel("Telefono");            //numero di telefono
	JTextField textTel = new JTextField(50);
	JLabel labelAltri = new JLabel("Altri...");		//eventuali altri campi
	
	boolean accettato = false;
	
	Persona nuovaPers = null;
	
	public MainWindow main;
	
	public FormWindow(MainWindow main){
		
		this.main = main;
		preparaGUI();
		setCampi();
		setBottoni();		
	} //fine FormWindow

	
	private void preparaGUI(){
		
		formDialog.setTitle("Rubrica - Editor Contatti");
		formContainer.setLayout(null);
		formDialog.setBounds(300, 300, 640, 480);
		formDialog.setVisible(true);
		formDialog.setResizable(false);
		formDialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		formDialog.addWindowListener(
			new WindowAdapter() {
					
				public void windowClosing(WindowEvent e){
					chiudiForm();
				}
		});
		
		//pannello menu: qui andr� una JMenuBar
		panelMenu.setLayout(null);
		panelMenu.setBounds(0, 0, 640, 20);
		labMenu.setHorizontalAlignment(SwingConstants.CENTER);
		labMenu.setBounds(0, 0, 640, 20);
		labMenu.setFont(new Font(labMenu.getFont().getName(),Font.BOLD,20));
		panelMenu.add(labMenu);
		
		//pannello form
		panelForm.setLayout(null);
		panelForm.setBounds(0, 30, 640, 350);
		
		//pannello contenitore dei bottoni
		panelBottoni.setLayout(null);
		panelBottoni.setBounds(0, 400, 640, 20);
		
		formContainer.add(panelMenu);
		formContainer.add(panelForm);
		formContainer.add(panelBottoni);		
	} //fine preparaGUI
	
	private void setCampi(){
		
		int gap = 40;
		int lposx = 30, tposx = 120;  // posizione x di label e textfield
		int posy = 20;
		int width = 120;
		int height = 20;
		
		//dimensione campi di inserimento dati
		labelCognome.setBounds(lposx, posy, width, height);
		textCognome.setBounds(tposx, posy,3*width, height);
		
		labelNome.setBounds(lposx, posy + gap, width, height);
		textNome.setBounds(tposx, posy + gap,3*width, height);
		
		labelEta.setBounds(lposx, posy + 2*gap, width, height);
		textEta.setBounds(tposx, posy + 2*gap,3*width, height);
		textEta.setToolTipText("Inserire solo numeri");
		textEta.addKeyListener(new keyListener());
		
		labelIndirizzo.setBounds(lposx, posy + 3*gap, width, height);
		textIndirizzo.setBounds(tposx, posy + 3*gap, 3*width, height);
		
		labelTel.setBounds(lposx, posy + 4*gap, width, height);
		textTel.setBounds(tposx, posy + 4*gap,3*width, height);
		textTel.setToolTipText("Inserire solo numeri");
		textTel.addKeyListener(new keyListener());
		
		labelAltri.setBounds(lposx, posy + 5*gap, width, height);
		
		//aggiungo campi al pannello campi
		panelForm.add(labelCognome);
		panelForm.add(textCognome);
		panelForm.add(labelNome);
		panelForm.add(textNome);
		panelForm.add(labelEta);
		panelForm.add(textEta);
		panelForm.add(labelIndirizzo);
		panelForm.add(textIndirizzo);
		panelForm.add(labelTel);
		panelForm.add(textTel);
		panelForm.add(labelAltri);		
	} //fine setCampi
	
	private void setBottoni(){
		
		int width = 100;
		int height = 20;
		
		accetta.setBounds((640-2*width) - 40, 0, width, height);
		annulla.setBounds((640 - width) - 30, 0, width, height);
		
		panelBottoni.add(accetta);
		panelBottoni.add(annulla);
		
		annulla.addActionListener(new Listener());
		
		accetta.addActionListener(new Listener());		
	} //fine setBottoni
	
	public void chiudiForm(){
		
		if(!accettato){					
			int scelta = JOptionPane.showConfirmDialog(null, "Vuoi uscire senza applicare le modifiche?");
				if(scelta != JOptionPane.YES_OPTION){
				return;
				}
		}
		
		main.formCreata = 0;
		formDialog.dispose();		
	} //fine chiudiForm
	
	public boolean nuovaPersona(){
								
		if(!textCognome.getText().isEmpty() && !textNome.getText().isEmpty() && !textIndirizzo.getText().isEmpty() && !textTel.getText().isEmpty() && !textEta.getText().isEmpty()){
			 nuovaPers = new Persona(textCognome.getText(),textNome.getText(), textIndirizzo.getText(), textTel.getText(), Integer.parseInt(textEta.getText()));
			 if(main.formCreata == 2)
				 main.contatti.remove(main.table.getSelectedRow());   // in caso di modifica, elimino la riga selezionata e aggiungo la nuova
			 main.contatti.add(nuovaPers);
			 main.model.setRowCount(0);
			 Collections.sort(main.contatti);
			 main.leggiArray();
			 main.salvato = false;  // ci sono modifiche nella rubrica (eventualmente da salvare)
			 accettato = true;     // le modifiche sono accettate
			 return true;
		}
		
		else if(textCognome.getText().isEmpty()){
			JOptionPane.showMessageDialog(formDialog, "Nessun cognome inserito!");
		}
		else if(textNome.getText().isEmpty()){
				JOptionPane.showMessageDialog(formDialog, "Nessun nome inserito!");
		}
		else if(textIndirizzo.getText().isEmpty()){
				JOptionPane.showMessageDialog(formDialog, "Nessun indirizzo inserito!");
		}
		else if(textTel.getText().isEmpty()){
				JOptionPane.showMessageDialog(formDialog, "Nessun telefono inserito!");
		}
		else if(textEta.getText().isEmpty()){
				JOptionPane.showMessageDialog(formDialog, "Nessuna et� inserita!");
		}
		
		return false;						
	} //fine nuovaPersona
	
	public boolean modificaPersona(){
		
		int index = main.table.getSelectedRow();
		
		textCognome.setText(main.contatti.get(index).getCognome());
		textNome.setText(main.contatti.get(index).getNome());
		textIndirizzo.setText(main.contatti.get(index).getIndirizzo());
		textTel.setText(main.contatti.get(index).getTel());
		textEta.setText(Integer.toString(main.contatti.get(index).getEta()));
		
		return false;		
	} //fine modificaPersona
		
	//classe non anonima per gestione degli eventi
	class Listener implements ActionListener{
		
		public void actionPerformed(ActionEvent event){
			
			Object source = event.getSource();
			
			if(source == annulla){
				chiudiForm();	
			}
						
			if(source == accetta){
				
				if(nuovaPersona()){  // esci dalla form solo se la nuova persona � stata creata o modificata (completa)
					chiudiForm();
				}
			}			
		} //fine actionPerformed
		
	} //fine classe Listener
	
	class keyListener implements KeyListener{
		
		public void keyTyped(KeyEvent e){
			
			if("0123456789 ".indexOf(e.getKeyChar()) < 0) e.consume();
			
		}

		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}		
	} //fine classe keyListener
	
} // fine classe FormWindow
