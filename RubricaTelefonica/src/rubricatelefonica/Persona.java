package rubricatelefonica;

public class Persona implements Comparable<Persona> {

	/* informazioni dei contatti */
	
	private String nome;
	private String cognome;
	private int eta;
	private String indirizzo;
	private String tel;
	
	public Persona(String cognome, String nome, String indirizzo, String tel,
			int eta) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.eta = eta;
		this.indirizzo = indirizzo;
		this.tel = tel;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getEta() {
		return eta;
	}

	public void setEta(int eta) {
		this.eta = eta;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public int compareTo(Persona pComp) {
		// TODO Auto-generated method stub
		return this.cognome.compareTo(pComp.cognome);
	}
	
	
} /* fine classe Persona */
