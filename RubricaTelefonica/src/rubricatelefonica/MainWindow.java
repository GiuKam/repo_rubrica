package rubricatelefonica;


import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

public class MainWindow {
	
	JFrame mainFrame = new JFrame();
	Container mainContainer = mainFrame.getContentPane();
	JButton aggiungi = new JButton("Nuovo");
	JButton modifica = new JButton("Modifica");
	JButton elimina = new JButton("Elimina");
	JButton esci = new JButton("Esci");
	JButton salva = new JButton("Salva");
	JPanel panelBottoni = new JPanel();
	JPanel panelMenu = new JPanel();
	JPanel panelTable = new JPanel();
	JLabel labMenu = new JLabel("Rubrica - versione base");
	DefaultTableModel model = new DefaultTableModel(0, 3);
	JTable table = new JTable(model);
	JScrollPane tableScroll  = new JScrollPane(table);
	ArrayList<Persona> contatti = new ArrayList<Persona>();
	Persona nuovaPers = null;
	
	public FormWindow form;
	public int formCreata = 0;      //flag di controllo della creazione seconda finestra (0 - non creata; 1 - finestra aggiungi; 2 - finestra modifica)
	public boolean salvato = true;
	
	String f_location = "informazioni.txt";
	File file_rubrica = new File(f_location);
	
	public MainWindow() throws IOException{
		
		preparaGUI();
		setBottoni();
		leggiContatti(file_rubrica);
		setTable();		
	} //fine MainWindow
	
	// prepare GUI
	private void preparaGUI(){
		
		mainFrame.setTitle("Rubrica - Finestra Principale");
		mainContainer.setLayout(null);
		mainFrame.setBounds(150, 150, 700, 480);
		mainFrame.setVisible(true);
		mainFrame.setResizable(false);
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(
			new WindowAdapter() {
					
				public void windowClosing(WindowEvent e){	
					if(!salvato){					
						int scelta = JOptionPane.showConfirmDialog(null, "La rubrica � stata modificata. Vuoi uscire senza salvare?");
							if(scelta == JOptionPane.YES_OPTION)
							System.exit(0);
					}					
					else System.exit(0);
					}
		});
		
		//pannello menu: qui andr� una JMenuBar
		panelMenu.setLayout(null);
		panelMenu.setBounds(0, 0, 700, 20);
		labMenu.setHorizontalAlignment(SwingConstants.CENTER);
		labMenu.setBounds(0, 0, 700, 20);
		labMenu.setFont(new Font(labMenu.getFont().getName(),Font.BOLD,20));
		panelMenu.add(labMenu);
		
		//pannello contenitore dei bottoni
		panelBottoni.setLayout(null);
		panelBottoni.setBounds(0, 400, 700, 20);
		
		//pannello della tabella
		panelTable.setLayout(null);
		panelTable.setBounds(0, 30, 700, 350);
		
		mainContainer.add(panelBottoni);
		mainContainer.add(panelMenu);
		mainContainer.add(panelTable);		
	} //fine preparaGUI
	
	private void setTable(){
				
		model.setColumnIdentifiers(new String[]{"Cognome","Nome","Telefono"});
		tableScroll.setBounds(20, 10, 640, 330);
		leggiArray();

		panelTable.add(tableScroll);		
	} //fine setTable
	
	
	private void setBottoni(){
		
		int width = 100;
		int height = 20;
		
		aggiungi.setBounds(20,0,width,height);
		modifica.setBounds(width + 30, 0, width,height);
		elimina.setBounds(2*width + 40, 0, width,height);
		salva.setBounds((700-2*width) - 5, 0, width-30, height);
		esci.setBounds((700 - width) - 10, 0, width - 30, height);
						
		panelBottoni.add(aggiungi);
		panelBottoni.add(modifica);
		panelBottoni.add(elimina);
		panelBottoni.add(salva);
		panelBottoni.add(esci);
		
        esci.addActionListener(new Listener());
		
		aggiungi.addActionListener(new Listener());
		
		modifica.addActionListener(new Listener());
		
		elimina.addActionListener(new Listener());
		
		salva.addActionListener(new Listener());			
	} //fine setBottoni
	
	private void leggiContatti(File file) throws IOException{
		
		//se il file di contatti non esiste, lascia vuota la lista di contatti
		if(!(file.exists())){ 
			JOptionPane.showMessageDialog(mainFrame, "Nessun file preesistente, la rubrica � vuota.");			
		}		
		 // altrimenti leggi il file riga per riga, separa i ; e crea una nuova persona da inserire		
		else {                 
			FileReader f = new FileReader(file);

			try {
				BufferedReader b = new BufferedReader(f);
				String s = "";
				while(true){
					s = b.readLine();
					if (s == null) break;	
					String[] info = s.split(";");
					Persona p = new Persona(info[1], info[0], info[2], info[3], Integer.parseInt(info[4]) );	  // nel file troviamo nome;cognome;via;telefono;eta
					contatti.add(p);
				}

				b.close();
				Collections.sort(contatti);   // ordina per cognome (come previsto nella classe Persona) l'array di Persone

			} catch (Exception e) {
				System.out.println(e);
			}
		}
	} //fine leggiContatti
	
	public void leggiArray(){
		
		for(Persona p: contatti){
			model.addRow(new String[]{p.getCognome(),p.getNome(),p.getTel()});			
		}		
	}
	
	private void aggiungiContatto(){
		
		if(formCreata == 0){ 
			form = new FormWindow(this);
			formCreata = 1;
		}		
	} //fine aggiungiContatto
	
	private void modificaContatto(){
		
		ListSelectionModel selected = table.getSelectionModel();
		
		if(selected.isSelectionEmpty()){
			JOptionPane.showMessageDialog(mainFrame, "Nessuna riga selezionata!");
			return;
		}
		
		if(formCreata == 0){ 
			form = new FormWindow(this);
			formCreata = 2;
			form.modificaPersona();
		}		
	}  //fine modificaContatto
	
	private void eliminaContatto(){
		
		ListSelectionModel selected = table.getSelectionModel();
		
		if(selected.isSelectionEmpty())
			JOptionPane.showMessageDialog(mainFrame, "Nessuna riga selezionata!");		
		else{
			
			int scelta = JOptionPane.showConfirmDialog(null, "Eliminare il contatto "+contatti.get(table.getSelectedRow()).getCognome()+ " "+contatti.get(table.getSelectedRow()).getNome()+"?");
			if(scelta == JOptionPane.YES_OPTION){

				contatti.remove(table.getSelectedRow());
				salvato = false;
				model.setRowCount(0);
				leggiArray();
			}			
		}		
	} //fine eliminaContatto
	
	private void salvaRubrica() throws IOException{
		
		int scelta = JOptionPane.showConfirmDialog(mainFrame, "Vuoi salvare la rubrica?");
		
		if(scelta == JOptionPane.YES_OPTION){
			
			if(!file_rubrica.exists())
				file_rubrica.createNewFile();
			
			PrintStream ps = new PrintStream(file_rubrica);
			
			for(Persona p: contatti){
				
				String voce = p.getNome()+";"+p.getCognome()+";"+p.getIndirizzo()+";"+p.getTel()+";"+p.getEta();

				ps.println(voce);				
			}
			
			ps.close();
			salvato = true;
		}
	} //fine salvaRubrica
	
	
	
	//classe non anonima per gestione degli eventi
	class Listener implements ActionListener{
		
		
		public void actionPerformed(ActionEvent event){
			
			Object source = event.getSource();
			
			if(source == esci){
				if(!salvato){					
					int scelta = JOptionPane.showConfirmDialog(null, "La rubrica � stata modificata. Vuoi uscire senza salvare?");
					if(scelta == JOptionPane.YES_OPTION)
						System.exit(0);					
				}
				else System.exit(0);					
			}
			
			if(source == aggiungi)			
				aggiungiContatto();
			
			if(source == modifica)						
				modificaContatto();
			
			if(source == elimina)						
				eliminaContatto();
			
			if(source == salva)
				try {
					salvaRubrica();
				} catch (IOException e) {
					System.out.println(e);
				}
			
		} //fine actionPerformed
		
	} //fine classe Listener
	

} //fine classe MainWindow
